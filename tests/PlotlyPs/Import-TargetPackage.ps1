$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\..\" | Resolve-Path

$PackageName = "PlotlyPs"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"

Import-Module -Name $PackagePath -Force
