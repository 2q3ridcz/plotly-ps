﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "Add-PlotlyBarData" {
    Context "Unit test" {
        It "outputs empty graph data when no parameters set" {
            $PlotlyObject = New-PlotlyObject

            $Result =
            $PlotlyObject |
            Add-PlotlyBarData

            $Expect = '{"type":"bar"}'

            $Result.data | ConvertTo-Json -Depth 100 -Compress | Should Be $Expect
            $PlotlyObject.layout | ConvertTo-Json -Depth 100 -Compress | Should Be ($Result.layout | ConvertTo-Json -Depth 100 -Compress)
            $PlotlyObject.config | ConvertTo-Json -Depth 100 -Compress | Should Be ($Result.config | ConvertTo-Json -Depth 100 -Compress)
        }
    }
}
