﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "New-PlotlyComparisonLineChartDateXTime" {
    Context "No parameter" {
        It "Returns <Expect> when TargetName is <TargetName>" {
            $PlotlyObject = New-PlotlyComparisonLineChartDateXTime
            $PlotlyObject.data[0].X | Should BeNullOrEmpty
            $PlotlyObject.data[1].X | Should BeNullOrEmpty
            $PlotlyObject.data[0].y | Should BeNullOrEmpty
            $PlotlyObject.data[1].y | Should BeNullOrEmpty
        }
    }

    Context "Unit test" {
        It "Sets dtick of xaxis when -XDTick is set" {
            $PlotlyObject =
            New-PlotlyComparisonLineChartDateXTime `
                -TargetXDate @([datetime]::new(2000,1,1)) `
                -TargetYTime @([datetime]::new(1,1,1,8,10,0)) `
                -CompareXDate @([datetime]::new(2000,1,1)) `
                -CompareYTime @([datetime]::new(1,1,1,8,40,0)) `
                -XDTick 86400000

            $PlotlyObject.layout.xaxis.dtick | Should Be 86400000
        }

        It "Does not set dtick of xaxis when -XDTick is not set" {
            $PlotlyObject =
            New-PlotlyComparisonLineChartDateXTime `
                -TargetXDate @([datetime]::new(2000,1,1)) `
                -TargetYTime @([datetime]::new(1,1,1,8,10,0)) `
                -CompareXDate @([datetime]::new(2000,1,1)) `
                -CompareYTime @([datetime]::new(1,1,1,8,40,0))

            $PlotlyObject.layout.xaxis.dtick | Should Be $Null
        }
    }
}
