﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path


function Assert-PlotlyHtmlFile {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True)]
        [object]
        $ResultFilePath
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $ExpectFilePath
        ,
        [Parameter(Mandatory=$False)]
        [switch]
        $UpdateExpectFile
    )

    begin {}

    process {
        If ($UpdateExpectFile) { Copy-Item -Path $ResultFilePath -Destination $ExpectFilePath }

        $Expect = Get-Content -Path $ExpectFilePath -Encoding "utf8" | ?{$_ -Ne ""}
        $Result = Get-Content -Path $ResultFilePath -Encoding "utf8" | ?{$_ -Ne ""}

        $Result.Length | Should Be $Expect.Length
        foreach ($i in (0..($Result.Length - 1))) {
            $Result[$i] | Should Be $Expect[$i]
        }
    }

    end {}
}
function Assert-PlotlyObjectWithExpectPlotlyHtmlFile {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $FileName
        ,
        [Parameter(Mandatory=$False)]
        [switch]
        $UpdateExpectFile
    )

    begin {}

    process {
        $HtmlPath = "$TestDrive\$FileName"
        $PlotlyObject | New-PlotlyHtml | Out-File -FilePath $HtmlPath -Encoding "utf8"

        ### Start: Create sample html page
        foreach ($Version in $SampleVersions) {
            $PageFilePath = $SampleFolderPath | Join-Path -ChildPath $Version | Join-Path -ChildPath $FileName
            $PlotlyObject | New-PlotlyHtml -PlotlyUrl "https://cdn.plot.ly/plotly-$Version.min.js" | Out-File -FilePath $PageFilePath -Encoding "utf8"
        }
        ### End  : Create sample html page

        $APHFArgs = @{
            "ResultFilePath" = $HtmlPath
            "ExpectFilePath" = "$here\TestExpect\$FileName"
            "UpdateExpectFile" = $UpdateExpectFile
        }
        Assert-PlotlyHtmlFile @APHFArgs
    }

    end {}
}

Describe "PlotlyPs" {
    BeforeAll {
        $SampleVersions = @("1.50.0", "1.58.2", "2.8.3")

        $PublicFolderPath = "$here\..\..\tests_console\Output\public\"
        If (Test-Path -Path $PublicFolderPath  -PathType Container) {
            Remove-Item -Path $PublicFolderPath -Recurse
        }

        $SampleFolderPath = $PublicFolderPath | Join-Path -ChildPath "sample"

        foreach ($Version in $SampleVersions) {
            $VersionFolderPath = $SampleFolderPath | Join-Path -ChildPath $Version
            New-Item -Path $VersionFolderPath -ItemType Directory -Force | Out-Null
        }

        $PublicFolderPath = $PublicFolderPath | Resolve-Path
    }

    AfterAll {
        $PageFilePath = $PublicFolderPath | Join-Path -ChildPath "index.html"
        $PublicFolderUri = [Uri]::new($PublicFolderPath)

        $html = @()
        $html += "<html><head></head><body>"

        $html +=
        Get-ChildItem -Path $SampleFolderPath -Recurse -File |
        ?{$_.Extension -Eq ".html"} |
        %{
            $FileUri = [Uri]::new($_.FullName)
            $FileRelativeUri = $PublicFolderUri.MakeRelative($FileUri)
            "<a href='./" + $FileRelativeUri + "'>" + $_.Directory.Name + ": " + $_.Name + "</a><br/>"
        }

        $html += "</body></html>"
        $html | Out-File -FilePath $PageFilePath -Encoding "utf8"
    }

    Context "Plotly.js sample" {
        It "can reproduce https://plotly.com/javascript/axes/#toggling-axes-lines-ticks-labels-and-autorange" {
            $FileName = "axes---#toggling-axes-lines-ticks-labels-and-autorange.html"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData -X @(0, 1, 2, 3, 4, 5, 6, 7, 8) -Y @(8, 7, 6, 5, 4, 3, 2, 1, 0) |
            Add-PlotlyScatterData -X @(0, 1, 2, 3, 4, 5, 6, 7, 8) -Y @(0, 1, 2, 3, 4, 5, 6, 7, 8) |
            Set-PlotlyXAxis -AutoRange $True -ShowGrid $False -ZeroLine $False -ShowLine $False -AutoTick $True -Ticks "" -ShowTickLabels $False |
            Set-PlotlyYAxis -AutoRange $True -ShowGrid $False -ZeroLine $False -ShowLine $False -AutoTick $True -Ticks "" -ShowTickLabels $False

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/javascript/horizontal-bar-charts/#basic-horizontal-bar-chart" {
            $FileName = "horizontal-bar-charts---#basic-horizontal-bar-chart.html"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyBarData -X @(20, 14, 23) -Y @('giraffes', 'orangutans', 'monkeys') -Orientation 'h'

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/javascript/line-and-scatter/#line-and-scatter-plot" {
            $FileName = "line-and-scatter---#line-and-scatter-plot.html"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData -X @(1, 2, 3, 4) -Y @(10, 15, 13, 17) -Mode 'markers' |
            Add-PlotlyScatterData -X @(2, 3, 4, 5) -Y @(16, 5, 11, 9) -Mode 'lines' |
            Add-PlotlyScatterData -X @(1, 2, 3, 4) -Y @(12, 9, 15, 12) -Mode 'lines+markers'

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/javascript/line-and-scatter/#scatter-plot-with-a-color-dimension" {
            $FileName = "line-and-scatter---#scatter-plot-with-a-color-dimension.html"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData `
                -Y @(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5) `
                -Mode 'markers' `
                -MarkerSize 40 `
                -MarkerColor @(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39) |
            Set-PlotlyTitle -Text "Scatter Plot with a Color Dimension"

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/javascript/text-and-annotations/#multiple-annotations" {
            $FileName = "text-and-annotations---#multiple-annotations.html"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData `
                -X @(0, 1, 2, 3, 4, 5, 6, 7, 8) `
                -Y @(0, 1, 3, 2, 4, 3, 4, 6, 5) |
            Add-PlotlyScatterData `
                -X @(0, 1, 2, 3, 4, 5, 6, 7, 8) `
                -Y @(0, 4, 5, 1, 2, 2, 3, 4, 2) |
            Add-PlotlyAnnotation -X 2 -Y 5 -Xref 'x' -Yref 'y' -Text 'Annotation Text' -ShowArrow $True -ArrowHead 7 -Ax 0 -Ay -40 |
            Add-PlotlyAnnotation -X 4 -Y 4 -Xref 'x' -Yref 'y' -Text 'Annotation Text 2' -ShowArrow $True -ArrowHead 7 -Ax 0 -Ay -40

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/javascript/time-series/#date-strings" {
            $FileName = "time-series---#date-strings.html"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData `
                -X @('2013-10-04 22:23:00', '2013-11-04 22:23:00', '2013-12-04 22:23:00') `
                -Y @(1, 3, 6)

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
            }

        It "can reproduce https://plotly.com/javascript/time-series/#basic-time-series" {
            $FileName = "time-series---#basic-time-series.html"

            $Dataset = Import-Csv -Path "$here\TestDataset\finance-charts-apple.csv"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData `
                -Mode "lines" `
                -Name 'AAPL High' `
                -X $Dataset.Date `
                -Y $Dataset.'AAPL.High' `
                -LineColor '#17BECF' |
            Add-PlotlyScatterData `
                -Mode "lines" `
                -Name 'AAPL Low' `
                -X $Dataset.Date `
                -Y $Dataset.'AAPL.Low' `
                -LineColor '#7F7F7F' |
            Set-PlotlyTitle -Text 'Basic Time Series'

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/javascript/time-series/#manually-set-range" {
            $FileName = "time-series---#manually-set-range.html"

            $Dataset = Import-Csv -Path "$here\TestDataset\finance-charts-apple.csv"

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData `
                -Mode "lines" `
                -X $Dataset.Date `
                -Y $Dataset.'AAPL.High' `
                -LineColor '#17BECF' |
            Add-PlotlyScatterData `
                -Mode "lines" `
                -X $Dataset.Date `
                -Y $Dataset.'AAPL.Low' `
                -LineColor '#7F7F7F' |
            Set-PlotlyTitle -Text 'Custom Range' |
            Set-PlotlyXAxis -Type "date" -Range @('2016-07-01', '2016-12-31') |
            Set-PlotlyYAxis -AutoRange $True -Type "linear" -Range @(86.8700008333, 138.870004167)

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/python/gantt/#fd69a6c5-1d66-4efb-a6e8-7a27078d9b4e" {
            $FileName = "python---gantt---#fd69a6c5-1d66-4efb-a6e8-7a27078d9b4e.html"

            $Dataset = @(
                New-Object -TypeName psobject -Property @{"Task"="Job A"; "Start"='2009-01-01'; "Finish"='2009-02-28'}
                New-Object -TypeName psobject -Property @{"Task"="Job B"; "Start"='2009-03-05'; "Finish"='2009-04-15'}
                New-Object -TypeName psobject -Property @{"Task"="Job C"; "Start"='2009-02-20'; "Finish"='2009-05-30'}
            )

            $PlotlyObject =
            $Dataset |
            New-PlotlyTimeline `
                -XStart "Start" `
                -XEnd "Finish" `
                -Y "Task" |
            Set-PlotlyYAxis -AutoRange "reversed"

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }

        It "can reproduce https://plotly.com/python/gantt/#1026a56b-fca0-4a0b-96ae-fb9e7dea8d1a" {
            $FileName = "python---gantt---#1026a56b-fca0-4a0b-96ae-fb9e7dea8d1a.html"

            $Dataset = @(
                New-Object -TypeName psobject -Property @{"Task"="Job A"; "Start"='2009-01-01'; "Finish"='2009-02-28'; "Resource"="Alex"}
                New-Object -TypeName psobject -Property @{"Task"="Job B"; "Start"='2009-03-05'; "Finish"='2009-04-15'; "Resource"="Alex"}
                New-Object -TypeName psobject -Property @{"Task"="Job C"; "Start"='2009-02-20'; "Finish"='2009-05-30'; "Resource"="Max"}
            )

            $PlotlyObject =
            $Dataset |
            New-PlotlyTimeline `
                -XStart "Start" `
                -XEnd "Finish" `
                -Y "Resource" `
                -Color "Resource" |
            Set-PlotlyYAxis -AutoRange "reversed"

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }
    }

    Context "New-PlotlyObject sample" {
        It "draws scatter plot with color bar" {
            $FileName = "sample---scatter-plot-with-color-bar.html"

            $MarkerColor = @("2022-01-08","2022-01-09","2022-01-01","2022-01-10","2022-01-07","2022-01-02","2022-01-14","2022-01-06","2022-01-03","2022-01-15","2022-01-05","2022-01-12","2022-01-04","2022-01-11","2022-01-13")
            $MarkerColorTicks = $MarkerColor | Get-Date | %{$_.Ticks / 10000}
            $MarkerColorMin = $MarkerColor | Measure-Object -Minimum | %{$_.Minimum}
            $MarkerColorMax = $MarkerColor | Measure-Object -Maximum | %{$_.Maximum}

            $PlotlyObject =
            New-PlotlyObject |
            Add-PlotlyScatterData `
                -X @(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15) `
                -Y @(10,15,13,17,2,5,7,9,2,6,8,9,2,6,8) `
                -Mode 'markers' `
                -MarkerColor $MarkerColorTicks `
                -MarkerColorBarOrientation "h" `
                -MarkerColorBarOutlineWidth 0 `
                -MarkerColorBarTickMode "array" `
                -MarkerColorBarTickText @($MarkerColorMin, $MarkerColorMax) `
                -MarkerColorBarTickVals @(($MarkerColorMin | Get-Date | %{$_.Ticks / 10000}), ($MarkerColorMax | Get-Date | %{$_.Ticks / 10000}))

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }
    }

    Context "New-PlotlyTimeline sample" {
        It "draws timeline" {
            $FileName = "sample---timeline.html"

            $Dataset = @(
                New-Object -TypeName psobject -Property @{"Date"="2009-01-01"; "Task"="Job A"; "Start"='0001-01-01 12:00:00'; "Finish"='0001-01-01 13:00:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-01"; "Task"="Job B"; "Start"='0001-01-01 12:50:00'; "Finish"='0001-01-01 13:30:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-01"; "Task"="Job C"; "Start"='0001-01-01 13:20:00'; "Finish"='0001-01-01 13:50:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-02"; "Task"="Job A"; "Start"='0001-01-01 11:50:00'; "Finish"='0001-01-01 13:10:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-02"; "Task"="Job B"; "Start"='0001-01-01 13:16:00'; "Finish"='0001-01-01 13:40:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-02"; "Task"="Job C"; "Start"='0001-01-01 13:34:00'; "Finish"='0001-01-01 13:56:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-03"; "Task"="Job A"; "Start"='0001-01-01 12:11:00'; "Finish"='0001-01-01 12:56:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-03"; "Task"="Job B"; "Start"='0001-01-01 12:43:00'; "Finish"='0001-01-01 13:26:00'}
                New-Object -TypeName psobject -Property @{"Date"="2009-01-03"; "Task"="Job C"; "Start"='0001-01-01 13:26:00'; "Finish"='0001-01-01 13:56:00'}
            )

            $PlotlyObject =
            $Dataset |
            New-PlotlyTimeline `
                -XStart "Start" `
                -XEnd "Finish" `
                -Y "Date" `
                -Color "Task" `
                -Opacity 0.7 |
            Set-PlotlyXAxis -Type "date" -TickFormat "%H:%M" |
            Set-PlotlyYAxis -AutoRange "reversed" -Type "date" -TickFormat "%Y/%m/%d" -DTick 86400000 |
            Set-PlotlyBarMode -BarMode "overlay"

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }
    }

    Context "New-PlotlyComparisonLineChartDateXTime sample" {
        It "draws comparison line chart with date on xaxis and time on yaxis" {
            $FileName = "sample---comparison-line-chart-date-x-time.html"

            $Dataset = "Date,StartTime,MidTime,EndTime
                2021/1/1,3:38,4:47,5:06
                2021/1/2,5:12,5:15,5:30
                2021/1/3,5:54,6:10,6:14
                2021/1/4,5:46,6:00,6:04
                2021/1/5,6:08,6:24,6:39
                2021/1/6,5:43,6:01,6:20
                2021/1/7,5:14,5:31,5:47
                2021/1/8,5:16,5:19,5:34
                2021/1/9,5:33,5:36,5:47
                2021/1/10,6:09,6:28,6:38
                2021/1/11,5:40,5:53,6:01
                2021/1/12,5:54,5:56,5:57
                2021/1/13,6:09,6:10,6:17
                2021/1/14,5:33,5:39,5:41
                2021/1/15,6:08,6:09,6:25
                2021/1/16,5:09,5:25,5:28
                2021/1/17,5:02,5:04,5:21
                2021/1/18,6:29,6:47,6:50
                2021/1/19,5:06,5:15,5:34
                2021/1/20,6:48,6:52,7:05
                2021/1/21,6:19,6:38,6:50
                2021/1/22,6:23,6:41,6:48
                2021/1/23,6:39,6:52,7:10
                2021/1/24,6:48,7:02,7:05
                2021/1/25,6:56,7:05,7:15
                2021/1/26,5:52,6:03,6:03
                2021/1/27,5:50,6:06,6:15
                2021/1/28,6:27,6:40,6:56
                2021/1/29,6:12,6:31,6:45
                2021/1/30,6:17,6:35,6:43
                2021/1/31,5:31,5:44,5:52
                2022/1/1,6:41,6:59,7:01
                2022/1/2,5:21,5:30,5:42
                2022/1/3,5:35,5:38,5:48
                2022/1/4,6:48,6:56,7:13
                2022/1/5,6:39,6:54,7:08
                2022/1/6,6:57,6:57,7:10
                2022/1/7,5:25,5:38,5:50
                2022/1/8,6:28,6:30,6:34
                2022/1/9,6:21,6:21,6:34
            " | ConvertFrom-Csv

            $Dataset |
            %{
                $_.Date = [datetime]::Parse($_.Date)
                $_.StartTime = [datetime]::parse("0001-01-01 " + $_.StartTime)
                $_.MidTime = [datetime]::parse("0001-01-01 " + $_.MidTime)
                $_.EndTime = [datetime]::parse("0001-01-01 " + $_.EndTime)
            }

            $TargetDataset = $Dataset | ?{ $_.Date.Year -Eq 2022 }
            $CompareDataset = $Dataset | ?{ $_.Date.Year -Eq 2021 }
            $CompareDataset | %{ $_.Date = ([datetime]::new(2022,1,1) + ($_.Date - [datetime]::new(2021,1,1))) }

            $PlotlyObject =
            New-PlotlyComparisonLineChartDateXTime `
                -TargetName "TargetName" `
                -TargetXDate $TargetDataset.Date `
                -TargetYTime $TargetDataset.StartTime `
                -CompareName "CompareName" `
                -CompareXDate $CompareDataset.Date `
                -CompareYTime $CompareDataset.StartTime `
                -XDTick 86400000

            Assert-PlotlyObjectWithExpectPlotlyHtmlFile -PlotlyObject $PlotlyObject -FileName $FileName
        }
    }

    Context "New-PlotlyKPIDashboardHtml sample" {
        @("StartTime","EndTime","Timespan") |
        %{
            $ColumnName = $_

            It "creates $ColumnName KPI dashboard html" {
                $Dataset = "Date,StartTime,MidTime,EndTime
                    2021/1/1,3:38,4:47,5:06
                    2021/1/2,5:12,5:15,5:30
                    2021/1/3,5:54,6:10,6:14
                    2021/1/4,5:46,6:00,6:04
                    2021/1/5,6:08,6:24,6:39
                    2021/1/6,5:43,6:01,6:20
                    2021/1/7,5:14,5:31,5:47
                    2021/1/8,5:16,5:19,5:34
                    2021/1/9,5:33,5:36,5:47
                    2021/1/10,6:09,6:28,6:38
                    2022/1/1,6:41,6:59,7:01
                    2022/1/2,5:21,5:30,5:42
                    2022/1/3,5:35,5:38,5:48
                    2022/1/4,6:48,6:56,7:13
                    2022/1/5,6:39,6:54,7:08
                    2022/1/6,6:57,6:57,7:10
                " | ConvertFrom-Csv

                $FormattedDataset =
                $Dataset |
                %{
                    $Starttime = [datetime]::parse("0001-01-01 " + $_.StartTime)
                    $Endtime = [datetime]::parse("0001-01-01 " + $_.EndTime)
                    @{
                        "Date" = [datetime]::Parse($_.Date)
                        "StartTime" = $Starttime
                        "EndTime" = $Endtime
                        "Timespan" = [datetime]::new(($Endtime - $Starttime).Ticks)
                    }
                }

                $TargetDataset = $FormattedDataset | ?{ $_.Date.Year -Eq 2022 }
                $CompareDataset = $FormattedDataset | ?{ $_.Date.Year -Eq 2021 }
                $CompareDataset | %{ $_.Date = ([datetime]::new(2022,1,1) + ($_.Date - [datetime]::new(2021,1,1))) }

                $PlotlyObject =
                New-PlotlyComparisonLineChartDateXTime `
                    -TargetName "TargetName" `
                    -TargetXDate $TargetDataset.Date `
                    -TargetYTime $TargetDataset.($ColumnName) `
                    -CompareName "CompareName" `
                    -CompareXDate $CompareDataset.Date `
                    -CompareYTime $CompareDataset.($ColumnName) `
                    -XDTick 86400000


                $FileName = "sample---$ColumnName-kpi-dashboard-html.html".ToLower()
                $HtmlPath = "$TestDrive\$FileName"

                $KPIValue = $TargetDataset | ?{$_.Date -Eq [datetime]::new(2022,1,6)} | %{$_.($ColumnName).ToString("HH:mm")}

                $Splatter = @{
                    "KPIPlotlyObjects" = @($PlotlyObject,$PlotlyObject,$PlotlyObject)
                    "KPINames" = @("Process1 end time", "Process2 end time", "Process3 end time")
                    "KPIValues" = @($KPIValue, $KPIValue, $KPIValue)
                    "Title" = "Process end time dashboard"
                }

                New-PlotlyKPIDashboardHtml @Splatter |
                Out-File -FilePath $HtmlPath -Encoding "utf8"

                ### Start: Create sample html page
                foreach ($Version in $SampleVersions) {
                    $PageFilePath = $SampleFolderPath | Join-Path -ChildPath $Version | Join-Path -ChildPath $FileName
                    $Splatter["PlotlyUrl"] = "https://cdn.plot.ly/plotly-$Version.min.js"
                    New-PlotlyKPIDashboardHtml @Splatter |
                    Out-File -FilePath $PageFilePath -Encoding "utf8"
                }
                ### End  : Create sample html page

                $APHFArgs = @{
                    "ResultFilePath" = $HtmlPath
                    "ExpectFilePath" = "$here\TestExpect\$FileName"
                    "UpdateExpectFile" = $False
                }
                Assert-PlotlyHtmlFile @APHFArgs
            }
        }
    }

    Context "New-PlotlyDailyPeriodKPIDashboardHtml sample" {
        It "creates date-x-time KPI dashboard html" {
            $Dataset = "Date,StartTime,EndTime
                2021/1/1,0001/1/1 3:38,0001/1/1 4:47
                2021/1/2,0001/1/1 5:12,0001/1/1 5:15
                2021/1/3,0001/1/1 5:54,0001/1/1 6:10
                2021/1/4,0001/1/1 5:46,0001/1/1 6:00
                2021/1/5,0001/1/1 6:08,0001/1/1 6:24
                2021/1/6,0001/1/1 5:43,0001/1/1 6:01
                2021/1/7,0001/1/1 5:14,0001/1/1 5:31
                2021/1/8,0001/1/1 5:16,0001/1/1 5:19
                2021/1/9,0001/1/1 5:33,0001/1/1 5:36
                2021/1/10,0001/1/1 6:09,0001/1/1 6:28
                2022/1/1,0001/1/1 6:41,0001/1/1 6:59
                2022/1/2,0001/1/1 5:21,0001/1/1 5:30
                2022/1/3,0001/1/1 5:35,0001/1/1 5:38
                2022/1/4,0001/1/1 6:48,0001/1/1 6:56
                2022/1/5,0001/1/1 6:39,0001/1/1 6:54
                2022/1/6,0001/1/1 6:57,0001/1/1 6:57
                2022/1/7,0001/1/1 5:14,0001/1/1 5:31
            " | ConvertFrom-Csv

            foreach ($Rec in $Dataset) {
                foreach ($Col in @("Date","StartTime","EndTime")) {
                    $Rec.($Col) = [datetime]::parse($Rec.($Col))
                }
            }

            $FileName = "sample---daily-period-kpi-dashboard-html.html"
            $HtmlPath = "$TestDrive\$FileName"

            $Splatter = @{
                "DailyPeriodObject" = $Dataset
                "DateProperty" = "Date"
                "StartProperty" = "StartTime"
                "EndProperty" = "EndTime"
                "TargetPeriod" = @([datetime]::new(2022,1,1), [datetime]::new(2022,12,31))
                "ComparePeriod" = @([datetime]::new(2021,1,1), [datetime]::new(2021,12,31))
                "Title" = "Daily Period KPI Dashboard"
                "KPINames" = @("StartTime", "EndTime", "Timespan")
            }

            New-PlotlyDailyPeriodKPIDashboardHtml @Splatter |
            Out-File -FilePath $HtmlPath -Encoding "utf8"

            ### Start: Create sample html page
            foreach ($Version in $SampleVersions) {
                $PageFilePath = $SampleFolderPath | Join-Path -ChildPath $Version | Join-Path -ChildPath $FileName
                $Splatter["PlotlyUrl"] = "https://cdn.plot.ly/plotly-$Version.min.js"
                New-PlotlyDailyPeriodKPIDashboardHtml @Splatter |
                Out-File -FilePath $PageFilePath -Encoding "utf8"
            }
            ### End  : Create sample html page

            $APHFArgs = @{
                "ResultFilePath" = $HtmlPath
                "ExpectFilePath" = "$here\TestExpect\$FileName"
                "UpdateExpectFile" = $False
            }
            Assert-PlotlyHtmlFile @APHFArgs
        }
    }

    Context "New-PlotlyDailyTimeAndAmountKPIDashboardHtml sample" {
        It "creates date-x-time KPI dashboard html" {
            $Dataset = "Date,Time,Amount
                2021/1/1,0001/1/1 3:38,100
                2021/1/2,0001/1/1 5:12,200
                2021/1/3,0001/1/1 5:54,143
                2021/1/4,0001/1/1 5:46,175
                2021/1/5,0001/1/1 6:08,214
                2021/1/6,0001/1/1 5:43,863
                2021/1/7,0001/1/1 5:14,457
                2021/1/8,0001/1/1 5:16,134
                2021/1/9,0001/1/1 5:33,875
                2021/1/10,0001/1/1 6:09,357
                2022/1/1,0001/1/1 6:41,357
                2022/1/2,0001/1/1 5:21,247
                2022/1/3,0001/1/1 5:35,986
                2022/1/4,0001/1/1 6:48,246
                2022/1/5,0001/1/1 6:39,235
                2022/1/6,0001/1/1 6:57,654
                2022/1/7,0001/1/1 5:14,148
            " | ConvertFrom-Csv

            foreach ($Rec in $Dataset) {
                foreach ($Col in @("Date","Time")) {
                    $Rec.($Col) = [datetime]::parse($Rec.($Col))
                }
            }

            $FileName = "sample---daily-time-and-amount-kpi-dashboard-html.html"
            $HtmlPath = "$TestDrive\$FileName"

            $Splatter = @{
                "DailyTimeAndAmountObject" = $Dataset
                "DateProperty" = "Date"
                "TimeProperty" = "Time"
                "AmountProperty" = "Amount"
                "TargetPeriod" = @([datetime]::new(2022,1,1), [datetime]::new(2022,12,31))
                "ComparePeriod" = @([datetime]::new(2021,1,1), [datetime]::new(2021,12,31))
                "Title" = "KPI Dashboard"
                "KPINames" = @("time", "amount")
            }

            New-PlotlyDailyTimeAndAmountKPIDashboardHtml @Splatter |
            Out-File -FilePath $HtmlPath -Encoding "utf8"

            ### Start: Create sample html page
            foreach ($Version in $SampleVersions) {
                $PageFilePath = $SampleFolderPath | Join-Path -ChildPath $Version | Join-Path -ChildPath $FileName
                $Splatter["PlotlyUrl"] = "https://cdn.plot.ly/plotly-$Version.min.js"
                New-PlotlyDailyTimeAndAmountKPIDashboardHtml @Splatter |
                Out-File -FilePath $PageFilePath -Encoding "utf8"
            }
            ### End  : Create sample html page

            $APHFArgs = @{
                "ResultFilePath" = $HtmlPath
                "ExpectFilePath" = "$here\TestExpect\$FileName"
                "UpdateExpectFile" = $False
            }
            Assert-PlotlyHtmlFile @APHFArgs
        }
    }
}
