$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path
$SrcFolderPath = $ProjectFolderPath | Join-Path -ChildPath "src"

Describe 'Testing against PSSA rules' {
    Context 'PSSA Standard Rules' {
        $PSScriptAnalyzerExists = (Get-Module -ListAvailable PSScriptAnalyzer) -Ne $Null

        If ($PSScriptAnalyzerExists) {
            $SettingsPath = "$here\..\PSScriptAnalyzerSettings.psd1"
            If (Test-Path -Path $SettingsPath) {
                $analysis = Invoke-ScriptAnalyzer -Path $SrcFolderPath -Recurse -Settings $SettingsPath
            } Else {
                $analysis = Invoke-ScriptAnalyzer -Path $SrcFolderPath -Recurse
            }

            $scriptAnalyzerRules = Get-ScriptAnalyzerRule
            forEach ($rule in $scriptAnalyzerRules) {
                It "Should pass $rule" {
                    If ($analysis.RuleName -contains $rule) {
                        $analysis |
                        ? RuleName -EQ $rule -outvariable failures | Out-Default

                        $failures.Count | Should Be 0
                    }
                }
            }
        }
    }
}
