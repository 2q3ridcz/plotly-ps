﻿function New-PlotlyKPIDashboardHtml {
    [CmdletBinding()]
    [OutputType([String])]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True)]
        [object[]]
        $KPIPlotlyObjects
        ,
        [Parameter(Mandatory=$False)]
        [String[]]
        $KPINames = @()
        ,
        [Parameter(Mandatory=$False)]
        [String[]]
        $KPIValues = @()
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $PlotlyUrl = "plotly.min.js"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $CssUrl = ""
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $Title = ""
        ,
        [Parameter(Mandatory=$False)]
        [String[]]
        $Description = @("")
    )

    begin {}

    process {
        $KPICount = $KPIPlotlyObjects.Length
        If ($KPICount -Ne $KPINames.Length) {
            Throw ('Count unmatch. KPIPlotlyObjects: ' + $KPICount + ', KPINames: ' + $KPINames.Length)
        }
        If ($KPICount -Ne $KPIValues.Length) {
            Throw ('Count unmatch. KPIPlotlyObjects: ' + $KPICount + ', KPIValues: ' + $KPIValues.Length)
        }

        $KPIPlotlyJsonDicts = $KPIPlotlyObjects | ConvertTo-PlotlyJsonDict

        $LinkElementString = ''
        If ($PSBoundParameters.ContainsKey('CssUrl')) {
            $LinkElementString = '
    <link href="' + $CssUrl + '" rel="stylesheet" />'
        }

        $HtmlString = '<!doctype html>
<html lang="ja" data-color-mode="auto" data-light-theme="light" data-dark-theme="dark_dimmed">
<head>
    <meta charset="utf-8"/>
    <style type="text/css">
        table { border: 2px solid; border-collapse: collapse; }
        th { border: 2px solid; }
        td { border: 2px solid; }

        .kpiTitle { font-size: 200%; }
        .kpiValue { font-size: 300%; }
    </style>' + $LinkElementString + '
    <title>' + $Title + '</title>
</head>
<body>
    <h1>' + $Title + '</h1>
    <p>
    ' + ($Description -join "<br/>") + '
    <p>
    <script type="text/javascript">window.PlotlyConfig = { MathJaxConfig: "local" };</script>
    <script src="' + $PlotlyUrl + '"></script>
    <table style="width: 100%">
    <tr>
'

        $Width = [Math]::Ceiling(100 / $KPICount)
        foreach ($i in (0..($KPICount - 1))) {
            $HtmlString += '        <td style="width: ' + $Width + '%; text-align: center;">
        <span class="kpiTitle">' + $KPINames[$i] + '</span><br/>
        <span class="kpiValue">' + $KPIValues[$i] + '</span><br/>
        </td>
'
        }

        $HtmlString += '    </tr>
    <tr>
'

    foreach ($i in (0..($KPICount - 1))) {
        $DivId = 'graphDiv' + ($i+1).ToString("00")
        $HtmlString += '        <td>
        <div style="width:100%;">
            <div id="' + $DivId + '" class="plotly-graph-div" style="height:100%; width:100%;"></div>
        </div>
        </td>
'
    }

    $HtmlString += '    </tr>
    </table>
'

    foreach ($i in (0..($KPICount - 1))) {
        $DivId = 'graphDiv' + ($i+1).ToString("00")
        $HtmlString += '
<script type="text/javascript">
window.PLOTLYENV = window.PLOTLYENV || {};
if (document.getElementById("' + $DivId +'")) {
    Plotly.newPlot(
    "' + $DivId + '",
    ' + $KPIPlotlyJsonDicts[$i].data + ',
    ' + $KPIPlotlyJsonDicts[$i].layout + ',
    ' + $KPIPlotlyJsonDicts[$i].config + '
    )
};
</script>
'
    }

    $HtmlString += '
</body>
</html>
'
        $HtmlString | Write-Output
    }

    end {}
}
