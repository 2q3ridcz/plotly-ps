﻿function New-PlotlyComparisonLineChartDateXAmount {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$False)]
        [string]
        $TargetName = "Target"
        ,
        [Parameter(Mandatory=$False)]
        [datetime[]]
        $TargetXDate = @()
        ,
        [Parameter(Mandatory=$False)]
        [int[]]
        $TargetYAmount = @()
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $CompareName = "Compare"
        ,
        [Parameter(Mandatory=$False)]
        [datetime[]]
        $CompareXDate = @()
        ,
        [Parameter(Mandatory=$False)]
        [int[]]
        $CompareYAmount = @()
        ,
        [Parameter(Mandatory=$False)]
        [object]
        $XDTick = $False
    )

    begin {}
    process {}

    end {
        If ($PSBoundParameters.ContainsKey("XDTick")) {
            $XDTickFlg = $True
        } Else {
            $XDTickFlg = $False
        }

        New-PlotlyObject |
        Add-PlotlyScatterData `
            -Mode "lines" `
            -Name $CompareName `
            -X ($CompareXDate | %{$_.ToString("yyyy-MM-dd")}) `
            -Y $CompareYAmount `
            -LineColor 'silver' |
        Add-PlotlyScatterData `
            -Mode "lines" `
            -Name $TargetName `
            -X ($TargetXDate | %{$_.ToString("yyyy-MM-dd")}) `
            -Y $TargetYAmount `
            -LineColor 'blue' |
        %{
            If ($XDTickFlg) {
                $_ | Set-PlotlyXAxis -ShowGrid $False -ZeroLine $False -ShowLine $False -Type "date" -DTick $XDTick
            } Else {
                $_ | Set-PlotlyXAxis -ShowGrid $False -ZeroLine $False -ShowLine $False -Type "date"
            }
        } |
        Set-PlotlyYAxis -ShowGrid $False -ZeroLine $False -ShowLine $False |
        Set-PlotlyShowLegend -ShowLegend $False |
        Set-PlotlyMargin -B 40 -L 40 -Pad 0 -R 40 -T 40
    }
}
