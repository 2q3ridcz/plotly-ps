﻿function Set-PlotlyLegend {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Title = ""
    )

    begin {
    }

    process {
        $ParamNames = @()
        $TitleParamNames = @("Title")

        $NodeDict = @{}
        foreach ($ParamName in $ParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $NodeDict[$ParamName.ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }

        $TitleDict = @{}
        foreach ($ParamName in $TitleParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                If ($ParamName -Eq "Title") {
                    $TitleDict["text"] = Get-Variable -Name $ParamName -ValueOnly
                } Else {
                    $TitleDict[$ParamName.ToLower()] = Get-Variable -Name $ParamName -ValueOnly
                }
            }
        }
        If ($TitleDict.Count -Ne 0) {
            $NodeDict["title"] = New-Object -TypeName PsObject -Property $TitleDict
        }

        $Node = New-Object -TypeName PsObject -Property $NodeDict

        $HasObj = $Null -Ne ($PlotlyObject.layout | Get-Member | ?{ $_.Name -Eq "legend" })

        If ($HasObj) {
            $PlotlyObject.layout.legend = $Node
        } Else {
            $PlotlyObject.layout | Add-Member -MemberType NoteProperty -Name "legend" -Value $Node
        }
        $PlotlyObject
    }

    end {
    }
}