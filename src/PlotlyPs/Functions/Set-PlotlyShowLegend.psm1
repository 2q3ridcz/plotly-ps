﻿function Set-PlotlyShowLegend {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $ShowLegend = $True
    )

    begin {}

    process {
        $HasObj = $Null -Ne ($PlotlyObject.layout | Get-Member | ?{ $_.Name -Eq "showlegend" })

        If ($HasObj) {
            $PlotlyObject.layout.showlegend = $ShowLegend
        } Else{
            $PlotlyObject.layout | Add-Member -MemberType NoteProperty -Name "showlegend" -Value $ShowLegend
        }
        $PlotlyObject
    }

    end {}
}