﻿function New-PlotlyDailyTimeAndAmountKPIDashboardHtml {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSReviewUnusedParameter", "")]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [object[]]
        $DailyTimeAndAmountObject
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $DateProperty
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $TimeProperty
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $AmountProperty
        ,
        [Parameter(Mandatory=$True)]
        [ValidateCount(2,2)]
        [Datetime[]]
        $TargetPeriod
        ,
        [Parameter(Mandatory=$True)]
        [ValidateCount(2,2)]
        [Datetime[]]
        $ComparePeriod
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Title = "Daily Amount And Time KPI Dashboard"
        ,
        [Parameter(Mandatory=$False)]
        [String[]]
        $Description = @("")
        ,
        [Parameter(Mandatory=$False)]
        [ValidateCount(2,2)]
        [string[]]
        $KPINames = @("Amount", "Time")
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $PlotlyUrl = "plotly.min.js"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $CssUrl = ""
    )
    begin { $Dataset = @() }
    process { $Dataset += $DailyTimeAndAmountObject }
    end {
        $FormattedDataset =
        $Dataset |
        %{
            New-Object -TypeName psobject -Property @{
                "Date" = $_.($DateProperty)
                "Time" = $_.($TimeProperty)
                "Amount" = $_.($AmountProperty)
            }
        }

        $TargetDataset = $FormattedDataset | ?{ $TargetPeriod[0] -Le $_.Date } | ?{ $_.Date -Le $TargetPeriod[1] }
        $CompareDataset = $FormattedDataset | ?{ $ComparePeriod[0] -Le $_.Date } | ?{ $_.Date -Le $ComparePeriod[1] }

        $CompareDataset | %{ $_.Date = ($TargetPeriod[0] + ($_.Date - $ComparePeriod[0])) }

        $KPIPlotlyObjects = @()
        $KPIPlotlyObjects += %{
            New-PlotlyComparisonLineChartDateXTime `
                -TargetName "Target" `
                -TargetXDate $TargetDataset.Date `
                -TargetYTime $TargetDataset.Time `
                -CompareName "Compare" `
                -CompareXDate $CompareDataset.Date `
                -CompareYTime $CompareDataset.Time
        }
        $KPIPlotlyObjects += %{
            New-PlotlyComparisonLineChartDateXAmount `
                -TargetName "Target" `
                -TargetXDate $TargetDataset.Date `
                -TargetYAmount $TargetDataset.Amount `
                -CompareName "Compare" `
                -CompareXDate $CompareDataset.Date `
                -CompareYAmount $CompareDataset.Amount
        }

        $TargetMaxDate = $TargetDataset | Measure-Object -Property "Date" -Maximum | %{ $_.Maximum }

        $KPIValues = @(
            $TargetDataset |
            ?{$_.Date -Eq $TargetMaxDate} |
            %{
                $_.Time.ToString("HH:mm") | Write-Output
                $_.Amount | Write-Output
            }
        )

        $Splatter = @{
            "KPIPlotlyObjects" = $KPIPlotlyObjects
            "KPINames" = $KPINames
            "KPIValues" = $KPIValues
            "Title" = $Title
            "Description" = $Description
            "PlotlyUrl" = $PlotlyUrl
            "CssUrl" = $CssUrl
        }
        New-PlotlyKPIDashboardHtml @Splatter
    }
}
