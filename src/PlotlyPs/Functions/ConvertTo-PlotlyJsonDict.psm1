﻿function ConvertTo-PlotlyJsonDict {
    [CmdletBinding()]
    [OutputType([Hashtable])]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
    )

    begin {}

    process {
        If ($PlotlyObject.data.Length -Eq 0) {
            $DataString = '[]'
        } ElseIf ($PlotlyObject.data.Length -Eq 1) {
            $DataString = '[' + ($PlotlyObject.data | ConvertTo-Json -Depth 100 -Compress) + ']'
        } Else {
            $DataString = $PlotlyObject.data | ConvertTo-Json -Depth 100 -Compress
        }

        If ($PlotlyObject.layout.keys.Length -Eq 0) {
            $LayoutString = '{}'
        } Else {
            $LayoutString = $PlotlyObject.layout | ConvertTo-Json -Depth 100 -Compress
        }

        If ($PlotlyObject.layout.keys.Length -Eq 0) {
            $ConfigString = '{}'
        } Else {
            $ConfigString = $PlotlyObject.config | ConvertTo-Json -Depth 100 -Compress
        }

        @{
            "data" = $DataString
            "layout" = $LayoutString
            "config" = $ConfigString
        }
    }

    end {}
}
