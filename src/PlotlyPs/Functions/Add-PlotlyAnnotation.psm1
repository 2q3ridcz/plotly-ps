﻿function Add-PlotlyAnnotation {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet(0,1,2,3,4,5,6,7,8)]
        [int]
        $ArrowHead = 1
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $AX = 0
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $AY = 0
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $ShowArrow = $True
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Text = ""
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $X = 0
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $XRef = ""
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $Y = 0
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $YRef = ""
    )

    begin {
    }

    process {
        # $Annotation = @{
        #     "x" = $X
        #     "y" = $Y
        #     "text" = $Text
        #     "showarrow" = $Showarrow
        #     "arrowhead" = $ArrowHead
        # }
        # If (0 -Ne $AX) { $Annotation["ax"] = $AX }
        # If (0 -Ne $AY) { $Annotation["ay"] = $AY }
        $ParamNames = @("AX","AY","ArrowHead","ShowArrow","Text","X","Y")

        $NodeDict = @{}
        foreach ($ParamName in $ParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $NodeDict[$ParamName.ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }

        $Nodes = @() + $PlotlyObject.layout.annotations
        $Nodes += New-Object -TypeName PsObject -Property $NodeDict
        $PlotlyObject.layout.annotations = $Nodes
        $PlotlyObject
    }

    end {
    }
}