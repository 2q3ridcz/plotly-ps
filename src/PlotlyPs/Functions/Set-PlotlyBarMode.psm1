﻿function Set-PlotlyBarMode {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet("stack","group","overlay","relative")]
        [string]
        $BarMode = "group"
    )

    begin {
    }

    process {
        $HasObj = $Null -Ne ($PlotlyObject.layout | Get-Member | ?{ $_.Name -Eq "barmode" })

        If ($HasObj) {
            $PlotlyObject.layout.barmode = $BarMode
        } Else{
            $PlotlyObject.layout | Add-Member -MemberType NoteProperty -Name "barmode" -Value $BarMode
        }
        $PlotlyObject
    }

    end {
    }
}