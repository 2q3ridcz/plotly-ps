﻿# https://plotly.com/python-api-reference/generated/plotly.express.timeline.html
function New-PlotlyTimeline {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object[]]
        $Data
        ,
        [Parameter(Mandatory=$True, Position=1)]
        [string]
        $XStart
        ,
        [Parameter(Mandatory=$True, Position=2)]
        [string]
        $XEnd
        ,
        [Parameter(Mandatory=$True, Position=3)]
        [string]
        $Y
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Color = ""
        ,
        [Parameter(Mandatory=$False)]
        [ValidateRange(0,1)]
        [float]
        $Opacity = 1
    )

    begin {
        $Dataset = @()
    }

    process {
        $Dataset += $Data
    }

    end {
        If ($PSBoundParameters.ContainsKey("Color")) {
            $ColorFlg = $True
            $Grouped = @($Dataset | Group-Object -Property $Color | %{@{"Name" = $_.Name; "Group" = $_.Group}} )
        } Else {
            $ColorFlg = $False
            $Grouped = @(@{"Name" = ""; "Group" = $Dataset})
        }

        $PlotlyObject = New-PlotlyObject

        $Grouped |
        %{
            $GroupName = $_.Name
            $Group = $_.Group

            $X =
            $Group |
            %{ ((Get-Date -Date $_.($XEnd)) - (Get-Date -Date $_.($XStart))).TotalMilliseconds }

            $Splatter = @{
                "Base" =  ($Group | %{$_.($XStart)})
                "X" = $X
                "Y" = ($Group | %{$_.($Y)})
                "Orientation" = 'h'
                "HoverTemplate" = ("" + $XStart + "=%{base}<br>" + $XEnd + "=%{x}<br>" + $Y + "=%{y}<extra></extra>")
            }
            If ($ColorFlg -Eq $True) {
                $Splatter["Name"] = $GroupName
                $Splatter["LegendGroup"] = $GroupName
            }
            If ($PSBoundParameters.ContainsKey("Opacity")) {
                $Splatter["Opacity"] = $Opacity
            }

            $PlotlyObject = $PlotlyObject | Add-PlotlyBarData @Splatter
        }

        $PlotlyObject |
        Set-PlotlyXAxis -Type "date" |
        Set-PlotlyYAxis -Title $Y |
        Set-PlotlyLegend -Title $Color |
        Set-PlotlyHoverMode -HoverMode "closest"
    }
}
