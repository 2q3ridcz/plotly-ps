﻿function Add-PlotlyBarData {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [object[]]
        $Base = @()
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $HoverTemplate = ""
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $LegendGroup = ""
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Name = ""
        ,
        [Parameter(Mandatory=$False)]
        [ValidateRange(0,1)]
        [float]
        $Opacity = 1
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet("h","v")]
        [string]
        $Orientation = "v"
        ,
        [Parameter(Mandatory=$False)]
        [object[]]
        $X = @()
        ,
        [Parameter(Mandatory=$False)]
        [object[]]
        $Y = @()
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $YAxis = ""
    )

    begin {}

    process {
        $ParamNames = @("Base","HoverTemplate","LegendGroup","Name","Opacity","Orientation","X","Y","YAxis")

        $NodeDict = @{"type" = "bar"}
        foreach ($ParamName in $ParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $NodeDict[$ParamName.ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }

        $Nodes = @() + $PlotlyObject.data
        $Nodes += New-Object -TypeName PsObject -Property $NodeDict
        $PlotlyObject.data = $Nodes
        $PlotlyObject
    }

    end {}
}