﻿function Set-PlotlyTitle {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False, Position=1)]
        [string]
        $Text = ""
    )

    begin {
    }

    process {
        $Title = New-Object -TypeName PsObject -Property @{
            "text" = $Text
        }

        $TitleObj = $PlotlyObject.layout | Get-Member | ?{ $_.Name -Eq "title" }

        If ($Null -Eq $TitleObj) {
            $PlotlyObject.layout | Add-Member -MemberType NoteProperty -Name "title" -Value $Title
        } Else{
            $PlotlyObject.layout.title = $Title
        }
        $PlotlyObject
    }

    end {
    }
}