﻿function Set-PlotlyHoverMode {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet("x","y","closest",$false,"x unified","y unified")]
        [object]
        $HoverMode = "closest"
    )

    begin {
    }

    process {
        $HasObj = $Null -Ne ($PlotlyObject.layout | Get-Member | ?{ $_.Name -Eq "hovermode" })

        If ($HasObj) {
            $PlotlyObject.layout.hovermode = $HoverMode
        } Else{
            $PlotlyObject.layout | Add-Member -MemberType NoteProperty -Name "hovermode" -Value $HoverMode
        }
        $PlotlyObject
    }

    end {
    }
}