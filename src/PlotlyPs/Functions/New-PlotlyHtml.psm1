﻿function New-PlotlyHtml {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $PlotlyUrl = "plotly.min.js"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $Title = ""
        ,
        [Parameter(Mandatory=$False)]
        [String[]]
        $Description = @("")
    )

    begin {}

    process {
        # If ($PlotlyObject.data.Length -Eq 0) {
        #     $DataString = '[]'
        # } ElseIf ($PlotlyObject.data.Length -Eq 1) {
        #     $DataString = '[' + ($PlotlyObject.data | ConvertTo-Json -Depth 100 -Compress) + ']'
        # } Else {
        #     $DataString = $PlotlyObject.data | ConvertTo-Json -Depth 100 -Compress
        # }

        # If ($PlotlyObject.layout.keys.Length -Eq 0) {
        #     $LayoutString = '{}'
        # } Else {
        #     $LayoutString = $PlotlyObject.layout | ConvertTo-Json -Depth 100 -Compress
        # }

        # If ($PlotlyObject.layout.keys.Length -Eq 0) {
        #     $ConfigString = '{}'
        # } Else {
        #     $ConfigString = $PlotlyObject.config | ConvertTo-Json -Depth 100 -Compress
        # }

        $PlotlyJsonDict = $PlotlyObject | ConvertTo-PlotlyJsonDict
        $DataString = $PlotlyJsonDict.data
        $LayoutString = $PlotlyJsonDict.layout
        $ConfigString = $PlotlyJsonDict.config

        '<html>

<head>
    <meta charset="utf-8" />
    <title>' + $Title + '</title>
</head>

<body>
    <h1>' + $Title + '</h1>
    <p>
        ' + ($Description -join "<br/>") + '
    <p>
    <script type="text/javascript">window.PlotlyConfig = { MathJaxConfig: "local" };</script>
    <script src="' + $PlotlyUrl + '"></script>
    <div style="width:100%;">
        <div id="graphDiv" class="plotly-graph-div" style="height:100%; width:100%;"></div>
        <script type="text/javascript">
            window.PLOTLYENV = window.PLOTLYENV || {};
            if (document.getElementById("graphDiv")) {
                Plotly.newPlot(
                    "graphDiv",
                    ' + $DataString + ',
                    ' + $LayoutString + ',
                    ' + $ConfigString + '
                )
            };
        </script>
    </div>
</body>

</html>'
    }

    end {}
}
