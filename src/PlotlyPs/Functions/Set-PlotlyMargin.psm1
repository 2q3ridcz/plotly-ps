﻿function Set-PlotlyMargin {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $AutoExpand = $True
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $B = 80
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $L = 80
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $Pad = 0
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $R = 80
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $T = 80
    )

    begin {}

    process {
        $ParamNames = @("AutoExpand","B","L","Pad","R","T")

        $NodeDict = @{}
        foreach ($ParamName in $ParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $NodeDict[$ParamName.ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }

        $Node = New-Object -TypeName PsObject -Property $NodeDict

        $HasObj = $Null -Ne ($PlotlyObject.layout | Get-Member | ?{ $_.Name -Eq "margin" })

        If ($HasObj) {
            $PlotlyObject.layout.margin = $Node
        } Else {
            $PlotlyObject.layout | Add-Member -MemberType NoteProperty -Name "margin" -Value $Node
        }
        $PlotlyObject
    }

    end {}
}