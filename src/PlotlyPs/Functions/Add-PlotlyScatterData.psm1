﻿function Add-PlotlyScatterData {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Name = ""
        ,
        [Parameter(Mandatory=$False)]
        [object[]]
        $X = @()
        ,
        [Parameter(Mandatory=$False)]
        [object[]]
        $Y = @()
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $YAxis = ""
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Mode = ""
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $LineColor = ""
        ,
        [Parameter(Mandatory=$False)]
        [object]
        $MarkerColor = ""
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $MarkerSize = 1
        ,
        [Parameter(Mandatory=$False)]
        [object]
        $MarkerColorBarDTick = ""
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet("h","v")]
        [string]
        $MarkerColorBarOrientation = "v"
        ,
        [Parameter(Mandatory=$False)]
        [int]
        $MarkerColorBarOutlineWidth = 1
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $MarkerColorBarTickFormat = ""
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet("auto","linear","array")]
        [string]
        $MarkerColorBarTickMode = ""
        ,
        [Parameter(Mandatory=$False)]
        [string[]]
        $MarkerColorBarTickText = @()
        ,
        [Parameter(Mandatory=$False)]
        [object[]]
        $MarkerColorBarTickVals = @()
    )

    begin {}

    process {
        $ParamNames = @("Name","Mode","X","Y","YAxis")
        $LineParamNames = @("LineColor")
        $MarkerParamNames = @("MarkerColor","MarkerSize")
        $MarkerColorBarParamNames = @("MarkerColorBarDTick","MarkerColorBarOrientation","MarkerColorBarOutlineWidth","MarkerColorBarTickFormat","MarkerColorBarTickMode","MarkerColorBarTickText","MarkerColorBarTickVals")

        $NodeDict = @{ "type" = "scatter" }
        foreach ($ParamName in $ParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $NodeDict[$ParamName.ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }

        $LineDict = @{}
        foreach ($ParamName in $LineParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $LineDict[$ParamName.Substring("Line".Length).ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }
        If ($LineDict.Count -Ne 0) {
            $NodeDict["line"] = New-Object -TypeName PsObject -Property $LineDict
        }

        $MarkerDict = @{}
        foreach ($ParamName in $MarkerParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $MarkerDict[$ParamName.Substring("Marker".Length).ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }
        $MarkerColorBarDict = @{}
        foreach ($ParamName in $MarkerColorBarParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $MarkerColorBarDict[$ParamName.Substring("MarkerColorBar".Length).ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }
        If ($MarkerColorBarDict.Count -Ne 0) {
            $MarkerDict["colorbar"] = New-Object -TypeName PsObject -Property $MarkerColorBarDict
        }
        If ($MarkerDict.Count -Ne 0) {
            $NodeDict["marker"] = New-Object -TypeName PsObject -Property $MarkerDict
        }

        $Datas = @() + $PlotlyObject.data
        $Datas += New-Object -TypeName PsObject -Property $NodeDict
        $PlotlyObject.data = $Datas
        $PlotlyObject
    }

    end {}
}