﻿function New-PlotlyDailyPeriodKPIDashboardHtml {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSReviewUnusedParameter", "")]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, Position=0)]
        [object[]]
        $DailyPeriodObject
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $DateProperty
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $StartProperty
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $EndProperty
        ,
        [Parameter(Mandatory=$True)]
        [ValidateCount(2,2)]
        [Datetime[]]
        $TargetPeriod
        ,
        [Parameter(Mandatory=$True)]
        [ValidateCount(2,2)]
        [Datetime[]]
        $ComparePeriod
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Title = "KPI Dashboard"
        ,
        [Parameter(Mandatory=$False)]
        [String[]]
        $Description = @("")
        ,
        [Parameter(Mandatory=$False)]
        [ValidateCount(3,3)]
        [string[]]
        $KPINames = @("StartTime", "EndTime", "Timespan")
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $PlotlyUrl = "plotly.min.js"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $CssUrl = ""
    )

    begin {
        $Dataset = @()
    }

    process { $Dataset += $DailyPeriodObject }

    end {
        $KPIColumnNames = @("StartTime", "EndTime", "Timespan")

        $FormattedDataset =
        $Dataset |
        %{
            New-Object -TypeName psobject -Property @{
                "Date" = $_.($DateProperty)
                "StartTime" = $_.($StartProperty)
                "EndTime" = $_.($EndProperty)
                "Timespan" = [datetime]::new(($_.($EndProperty) - $_.($StartProperty)).Ticks)
            }
        }

        $TargetDataset = $FormattedDataset | ?{ $TargetPeriod[0] -Le $_.Date } | ?{ $_.Date -Le $TargetPeriod[1] }
        $CompareDataset = $FormattedDataset | ?{ $ComparePeriod[0] -Le $_.Date } | ?{ $_.Date -Le $ComparePeriod[1] }

        $CompareDataset | %{ $_.Date = ($TargetPeriod[0] + ($_.Date - $ComparePeriod[0])) }

        $KPIPlotlyObjects =
        $KPIColumnNames |
        %{
            New-PlotlyComparisonLineChartDateXTime `
                -TargetName "Target" `
                -TargetXDate $TargetDataset.Date `
                -TargetYTime $TargetDataset.($_) `
                -CompareName "Compare" `
                -CompareXDate $CompareDataset.Date `
                -CompareYTime $CompareDataset.($_)
        }

        $TargetMaxDate = $TargetDataset | Measure-Object -Property "Date" -Maximum | %{ $_.Maximum }

        $KPIValues =
        $KPIColumnNames |
        %{
            $ColumnName = $_
            $TargetDataset | ?{$_.Date -Eq $TargetMaxDate} | %{$_.($ColumnName).ToString("HH:mm")}
        }

        $Splatter = @{
            "KPIPlotlyObjects" = $KPIPlotlyObjects
            "KPINames" = $KPINames
            "KPIValues" = $KPIValues
            "Title" = $Title
            "Description" = $Description
            "PlotlyUrl" = $PlotlyUrl
            "CssUrl" = $CssUrl
        }

        New-PlotlyKPIDashboardHtml @Splatter
    }
}
