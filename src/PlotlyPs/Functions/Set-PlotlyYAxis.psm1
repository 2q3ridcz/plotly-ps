﻿function Set-PlotlyYAxis {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$True,
            ValueFromPipeline=$true,
            Position=0)]
        [object]
        $PlotlyObject
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet($True,$False,"reversed")]
        [object]
        $AutoRange = $True
        ,
        [Parameter(Mandatory=$False)]
        [object]
        $DTick = ""
        ,
        [Parameter(Mandatory=$False)]
        [object[]]
        $Range = @()
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $TickFormat = ""
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Title = ""
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet("-","linear","log","date","category","multicategory")]
        [string]
        $Type = "-"
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $ShowGrid = $True
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $ZeroLine = $True
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $ShowLine = $True
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $AutoTick = $True
        ,
        [Parameter(Mandatory=$False)]
        [ValidateSet("outside","inside","")]
        [string]
        $Ticks = "outside"
        ,
        [Parameter(Mandatory=$False)]
        [bool]
        $ShowTickLabels = $True
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Side = ""
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Overlaying = ""
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $YAxisSuffix = ""
    )

    begin {
    }

    process {
        $ParamNames = @("AutoRange","DTick","Range","TickFormat","Type","ShowGrid","ZeroLine","ShowLine","AutoTick","Ticks","ShowTickLabels","Side","Overlaying")
        $TitleParamNames = @("Title")

        $AxisDict = @{}
        foreach ($ParamName in $ParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $AxisDict[$ParamName.ToLower()] = (Get-Variable -Name $ParamName -ValueOnly).psobject.BaseObject
            }
        }

        $TitleDict = @{}
        foreach ($ParamName in $TitleParamNames) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                If ($ParamName -Eq "Title") {
                    $TitleDict["text"] = Get-Variable -Name $ParamName -ValueOnly
                } Else {
                    $TitleDict[$ParamName.ToLower()] = Get-Variable -Name $ParamName -ValueOnly
                }
            }
        }
        If ($TitleDict.Count -Ne 0) {
            $AxisDict["title"] = New-Object -TypeName PsObject -Property $TitleDict
        }

        $Axis = New-Object -TypeName PsObject -Property $AxisDict

        $ObjName = "yaxis" + [string]($YAxisSuffix)
        $HasObj = $Null -Ne ($PlotlyObject.layout | Get-Member | ?{ $_.Name -Eq $ObjName })

        If ($HasObj) {
            $PlotlyObject.layout.($ObjName) = $Axis
        } Else {
            $PlotlyObject.layout | Add-Member -MemberType NoteProperty -Name $ObjName -Value $Axis
        }
        $PlotlyObject
    }

    end {
    }
}