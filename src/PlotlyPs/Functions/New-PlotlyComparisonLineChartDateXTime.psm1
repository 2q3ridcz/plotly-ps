﻿function New-PlotlyComparisonLineChartDateXTime {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$False)]
        [string]
        $TargetName = "Target"
        ,
        [Parameter(Mandatory=$False)]
        [datetime[]]
        $TargetXDate = @()
        ,
        [Parameter(Mandatory=$False)]
        [datetime[]]
        $TargetYTime = @()
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $CompareName = "Compare"
        ,
        [Parameter(Mandatory=$False)]
        [datetime[]]
        $CompareXDate = @()
        ,
        [Parameter(Mandatory=$False)]
        [datetime[]]
        $CompareYTime = @()
        ,
        [Parameter(Mandatory=$False)]
        [object]
        $XDTick = $False
    )

    begin {}
    process {}

    end {
        If ($PSBoundParameters.ContainsKey("XDTick")) {
            $XDTickFlg = $True
        } Else {
            $XDTickFlg = $False
        }

        New-PlotlyObject |
        Add-PlotlyScatterData `
            -Mode "lines" `
            -Name $CompareName `
            -X ($CompareXDate | %{$_.ToString("yyyy-MM-dd")}) `
            -Y ($CompareYTime | %{$_.ToString("yyyy-MM-dd HH:mm:ss")}) `
            -LineColor 'silver' |
        Add-PlotlyScatterData `
            -Mode "lines" `
            -Name $TargetName `
            -X ($TargetXDate | %{$_.ToString("yyyy-MM-dd")}) `
            -Y ($TargetYTime | %{$_.ToString("yyyy-MM-dd HH:mm:ss")}) `
            -LineColor 'blue' |
        %{
            If ($XDTickFlg) {
                $_ | Set-PlotlyXAxis -ShowGrid $False -ZeroLine $False -ShowLine $False -Type "date" -DTick $XDTick
            } Else {
                $_ | Set-PlotlyXAxis -ShowGrid $False -ZeroLine $False -ShowLine $False -Type "date"
            }
        } |
        Set-PlotlyYAxis -ShowGrid $False -ZeroLine $False -ShowLine $False -Type "date" -TickFormat "%H:%M" |
        Set-PlotlyShowLegend -ShowLegend $False |
        Set-PlotlyMargin -B 40 -L 40 -Pad 0 -R 40 -T 40
    }
}
