# plotly-ps

Create plotly html files via powershell functions.

## Sample

You can view the samples created by test scripts:

https://2q3ridcz.gitlab.io/plotly-ps/

Test scripts are in here:

./tests/PlotlyPs/PlotlyPs.Tests.ps1
