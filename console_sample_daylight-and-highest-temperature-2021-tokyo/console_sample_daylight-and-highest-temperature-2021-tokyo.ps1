$here = Split-Path -Parent $MyInvocation.MyCommand.Path
Push-Location -Path $here

"# " + (Split-Path -Leaf $MyInvocation.MyCommand.Path) | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host

"## " + 'Start Logging...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$LogFolderPath = "$here\Log" | Resolve-Path
$LogFileName = (Get-Date).ToString("yyyyMMdd-HHmmss") + "_Log.txt"
$LogFilePath = $LogFolderPath | Join-Path -ChildPath $LogFileName
Start-Transcript -Path $LogFilePath


"## " + 'Import modules...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$ProjectFolderPath = "$here\..\" | Resolve-Path

foreach ($PackageName in @("PlotlyPs")) {
    $PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
    Import-Module -Name $PackagePath -Force
}


"## " + 'Creating HTML file...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$InputFolderPath = "$here\Input" | Resolve-Path
$OutputFolderPath = "$here\Output" | Resolve-Path

$CsvFilePath = $InputFolderPath | Join-Path -ChildPath "daylight-and-highest-temperature-2021-tokyo.csv"
$PageFilePath = $OutputFolderPath | Join-Path -ChildPath "daylight-and-highest-temperature-2021-tokyo.html"

$Data =
Import-Csv -Path $CsvFilePath -Encoding Default |
%{
    $_.Date = [datetime]::parse($_.Date)
    $_.HighestTemperatureTime = [datetime]::parse($_.HighestTemperatureTime)
    $_.DaylightAmount = [int]$_.DaylightAmount
    $_.HighestTemperature = [int]$_.HighestTemperature

    $AddMemberParameter = @{
        "InputObject" = $_
        "MemberType" = "NoteProperty"
        "Name" = "Time"
        "Value" = [datetime]::new(($_.HighestTemperatureTime - $_.Date).Ticks)
        "PassThru" = $True
    }
    Add-Member @AddMemberParameter
}


$Version = "1.50.0"
$PlotlyUrl = "https://cdn.plot.ly/plotly-$Version.min.js"

$BarDataParameter = @{
    "Name" = "DaylightAmount"
    "X" = ($Data.Date | %{$_.ToString("yyyy-MM-dd")})
    "Y" = $Data.DaylightAmount
    "YAxis" = "y1"
}

$ScatterDataParameter = @{
    "Mode" = "lines"
    "Name" = "HighestTemperatureTime"
    "X" = ($Data.Date | %{$_.ToString("yyyy-MM-dd")})
    "Y" = ($Data.Time | %{$_.ToString("yyyy-MM-dd HH:mm:ss")})
    "YAxis" = "y2"
}

New-PlotlyObject |
Add-PlotlyBarData @BarDataParameter |
Add-PlotlyScatterData @ScatterDataParameter |
Set-PlotlyYAxis -ShowGrid $False -ZeroLine $False -ShowLine $False |
Set-PlotlyYAxis -ShowGrid $False -ZeroLine $False -ShowLine $False -Type "date" -TickFormat "%H:%M" -Side "right" -Overlaying "y" -YAxisSuffix "2" |
Set-PlotlyShowLegend -ShowLegend $True |
Set-PlotlyMargin -B 40 -L 40 -Pad 0 -R 40 -T 40 |
New-PlotlyHtml -PlotlyUrl $PlotlyUrl |
Out-File -FilePath $PageFilePath -Encoding "utf8"


"## " + 'End Logging...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
Stop-Transcript

Read-Host -Prompt ("Fin! Press enter to exit...")