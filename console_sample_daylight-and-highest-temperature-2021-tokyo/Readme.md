# Sample_daylight-and-highest-temperature-2021-tokyo

使用方法を簡単に紹介。

2021年の東京の日毎の最高気温発生時刻と日合計全天日射量をグラフ化する。  
横軸に日付、折れ線グラフに時刻、棒グラフに日射量を表す。

## 引用文献

入力データ「daylight-and-highest-temperature-2021-tokyo.csv」は気象庁のページ [気象庁|過去の気象データ・ダウンロード](https://www.data.jma.go.jp/gmd/risk/obsdl/index.php) のデータを基に作成。


> 数値データ、簡単な表・グラフ等は著作権の対象ではありませんので、これらについては本利用ルールの適用はなく、自由に利用できます。

[気象庁 | 著作権・リンク・個人情報保護について](https://www.jma.go.jp/jma/kishou/info/coment.html) より。

ありがとうございます！
